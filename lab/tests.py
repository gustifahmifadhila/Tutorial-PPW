from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import *
from datetime import datetime

# Create your tests here.

class LabUnitTest(TestCase):
    
    def test_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_using_index_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_model(self):
        new_message = Message.objects.create(message = "test")
        counter = Message.objects.all().count()
        self.assertEqual(counter, 1)