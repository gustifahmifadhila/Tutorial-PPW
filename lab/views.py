from django.shortcuts import render
from .forms import *

# Create your views here.

def index(request):
    response = {}
    form = MessageForm
    response['form'] = form
    response['message_list'] = Message.objects.all()
    if(request.method == 'POST' and form.is_valid):
        message = Message(message = request.POST['message'])
        message.save()
    return render(request, 'index.html', response)