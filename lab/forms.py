from django import forms
from .models import *

class MessageForm(forms.Form):
    message = forms.CharField(label="Status", required=True, widget=forms.Textarea(attrs={'class':'form-control'}))