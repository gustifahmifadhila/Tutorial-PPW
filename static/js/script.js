$(document).ready(function(){
    var theme = 'a';
    $('#tombolA').click(function(){
        $('body').css({'font-family':'Playball', 'background-color':'aqua', 'color':'black'});
        $('.expand').css({'background-color':'blue'});
        theme = 'a';
    });
    $('#tombolB').click(function(){
        $('body').css({'font-family':'Spicy Rice', 'background-color':'black', 'color':'white'});
        $('.expand').css({'background-color':'red'});
        theme = 'b';
    });
    $('#tombolA').mouseenter(function(){
        $('body').css({'font-family':'Playball', 'background-color':'aqua', 'color':'black'});
        $('.expand').css({'background-color':'blue'});
    });
    $('#tombolB').mouseenter(function(){
        $('body').css({'font-family':'Spicy Rice', 'background-color':'black', 'color':'white'});
        $('.expand').css({'background-color':'red'});
    });
    $('#tombolB').mouseleave(function(){
        if(theme == 'a') {
            $('body').css({'font-family':'Playball', 'background-color':'aqua', 'color':'black'});
            $('.expand').css({'background-color':'blue'});
        }
    });
    $('#tombolA').mouseleave(function(){
        if(theme == 'b'){
            $('body').css({'font-family':'Spicy Rice', 'background-color':'black', 'color':'white'});
            $('.expand').css({'background-color':'red'});
        }
    });
})

$(document).ready(function(){
    var expanded;
    $('.hiddentext').slideUp();
    $('.expand').click(function(){
        $(expanded).parent().find('.hiddentext').slideUp();
        if(expanded == this) {
            expanded = null;
        }
        else {
            expanded = this;
            $(this).parent().find('.hiddentext').slideDown();
        }
    });
});
